﻿using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour {
    
    public GameObject bullet_prefab;
    float bulletImpulse = 20f;
    private bool pause;

    private void Start()
    {
        pause = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && pause == false)
        {
            pause = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && pause == true)
        {
            pause = false;
        }
    }

    // Update is called once per frame
    public void Shoot () {
        if (pause) return;
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
    }
}
