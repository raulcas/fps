﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour {

    public float maxDistance;
    public LayerMask mask;
    public LayerMask mask2;
    public LayerMask mask3;
    public LayerMask mask4;

    public int maxAmmo;
    public int currentAmmo;
    public int reloadAmmo;
    public float fireRate;
    public float hitForce;
    public float hitDamage;

    public bool isShooting;
    public bool isReloading;

    public float reloadTime;

    private bool paused;

    //private LineRenderer laserLine;

    [SerializeField] Text ammo;
    [SerializeField] Text reload;
    private Animator anim;
    [SerializeField] GameObject arma;
    public GameObject impactEffect;
    public GameObject groundEffect;
    public GameObject ironEffect;
    public GameObject dirtEffect;
    public ParticleSystem shootEffects;

    private void Start()
    {
        paused = false;
        isShooting = false;
        isReloading = false;
        currentAmmo = maxAmmo;
        //laserLine = GetComponent<LineRenderer> ();
        anim = arma.GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && paused == false)
        {
            paused = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && paused == true)
        {
            paused = false;
        }

        ammo.text = currentAmmo.ToString("00");
        reload.text = reloadAmmo.ToString("00");

        anim.SetBool("Reloading", isReloading);
    }

    public void Shoot()
    {
        if(isShooting || isReloading) return;
        if(currentAmmo <= 0) return;
        if (paused) return;

        Debug.Log ("Shoot");

        shootEffects.Play();

        isShooting = true;        
        currentAmmo--;

        Ray ray = Camera.main.ViewportPointToRay (new Vector3(0.5f, 0.5f, 0.0f));


        // Set the start position for our visual effect for our laser to the position of gunEnd
        //laserLine.enabled = true;
        //laserLine.SetPosition (0, Camera.main.transform.position);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, maxDistance, mask))
        {
            Debug.Log("Hit");
            Debug.Log(hit.transform.name);
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            //laserLine.SetPosition (1, hit.transform.position);

            Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
        }

        if (Physics.Raycast(ray, out hit, maxDistance, mask2))
        {
            Debug.Log("Hit");
            Debug.Log(hit.transform.name);
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            //laserLine.SetPosition (1, hit.transform.position);

            Instantiate(groundEffect, hit.point, Quaternion.LookRotation(hit.normal));
        }

        if (Physics.Raycast(ray, out hit, maxDistance, mask4))
        {
            Debug.Log("Hit");
            Debug.Log(hit.transform.name);
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            //laserLine.SetPosition (1, hit.transform.position);

            Instantiate(dirtEffect, hit.point, Quaternion.LookRotation(hit.normal));
        }

        if (Physics.Raycast(ray, out hit, maxDistance, mask3))
        {
            Debug.Log("Hit");
            Debug.Log(hit.transform.name);
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            //laserLine.SetPosition (1, hit.transform.position);

            Instantiate(ironEffect, hit.point, Quaternion.LookRotation(hit.normal));

        }
        else
        {
            //Transform t = Camera.main.transform;
            //t.Translate(ray.direction * 100, Space.World);
            //Debug.DrawRay(ray.origin, Camera.main.transform.forward * 100, Color.green); 
            //laserLine.SetPosition (1, Camera.main.transform*ray.direction*100);
        }
        
        StartCoroutine(WaitFireRate());
    }
    private IEnumerator WaitFireRate()
    {/*
        float timeCounter = 0;
        while(timeCounter < fireRate)
        {
            timeCounter += Time.deltaTime;
        }
        isShooting = false;

        yield return null;*/
        Debug.Log("Empieza la corutina");
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        //laserLine.enabled = false;
        Debug.Log("Termina la corutina");
    }

    public void Reload()
    {
        if(isReloading) return;
        if (paused) return;
        if (currentAmmo == 10) return;
        if (reloadAmmo <= 0) return;
        isReloading = true;
        

        StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);

        if((currentAmmo + reloadAmmo) <= 10)
        {
            currentAmmo += reloadAmmo;
            reloadAmmo = 0;
        }
        else if((currentAmmo + reloadAmmo) > 10)
        {
            reloadAmmo -= (10-currentAmmo);
            currentAmmo = maxAmmo;
        }
        isReloading = false;
    }
}
