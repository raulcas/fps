﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour {

    private PlayerMovement playerController;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private MouseCursor mouseCursor;
    private Gun gun;
    private BallShoot ballShoot;

    private int mapaMenu = 0;

    private bool pause;

    void Start ()
    {
        mapaMenu = 0;
        pause = false;
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        lookRotation = playerController.GetComponent<LookRotation>();

        gun = playerController.GetComponent<Gun> ();
        ballShoot = playerController.GetComponent<BallShoot> ();
        
        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }

    public void Update(){
        if(Input.GetKeyDown(KeyCode.Escape) && pause == false)
        {
            pause = true;
            mouseCursor.ShowCursor();
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && pause == true)
        {
            mouseCursor.HideCursor();
            pause = false;
        }

        //El movimiento del player
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if(Input.GetButton("Jump")) playerController.StartJump();

        
        //Rotación de la cámara
        Vector2 mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);

        //Cursor del ratón      

        if(pause == false)
        {
            if (Input.GetMouseButton(0)) gun.Shoot();
            if (Input.GetKeyDown(KeyCode.R)) gun.Reload();

            if (Input.GetMouseButtonDown(1)) ballShoot.Shoot();
        }
    }     
}

